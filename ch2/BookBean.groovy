class BookBean {
    String title // property declaration
}

def groovyBook = new BookBean()

// The setter and getter methods are auto-generated
groovyBook.setTitle("Groovy in Action")
assert groovyBook.getTitle() == "Groovy in Action"

// This looks like direct field access but is just Groovy sugar
// for the setter method, the actual field is private
groovyBook.title = 'Groovy conquers the world'
assert groovyBook.getTitle() == 'Groovy conquers the world'