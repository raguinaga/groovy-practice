@Grab('commons-lang:commons-lang:3.11')
import org.apache.commons.lang3.ClassUtils

class Outer { class Inner {} }

assert !ClassUtils.isInnerClass(Outer)
assert ClassUtils.isInnerClass(Outer.Inner)