import groovy.transform.Immutable

// This class becomes final, along with all its fields
// its state cannot changed after construction
@Immutable class FixedBook { // AST annotation
    String title
}

def gina = new FixedBook('Groovy in Action') // positional constructor
def regina = new FixedBook(title: 'Groovy in Action') // named-arg constructor

assert gina.title == 'Groovy in Action'
asssert gina == regina

try {
    gina.title = 'Oops!'
    assert false, "should not reach here"
} catch (ReadOnlyPropertyException expected) {
    println "Expected error: '$expected.message'"
}